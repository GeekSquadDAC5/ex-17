﻿using System;

namespace ex_17
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #17
            // --------------------------------------------
            // We haven't mentioned the if statement yet when it comes to coding.
            // But be adventurous  - try and find out how to do an if statement.
            // Use Google and see what you can find.
            // Create a simple app that uses an if statement.
            // Ask a simple question which has a true / false answer and let the user choose an option.
            
            // Set a variable needed.
            var userBirthMonth = "";
            var userAnswerOfBirthMonth ="";
            Boolean isFished = false;

            // Get user's birthday
            Console.WriteLine("-----------------------------------");
            Console.WriteLine(" Let me ask you some information.");
            Console.WriteLine("-----------------------------------");

            while(!isFished)
            {
                Console.Write("What month are you born in (ex:March or 03)?  : ");
                userBirthMonth = Console.ReadLine();
                Console.WriteLine($"\nYour birth month is {userBirthMonth}\n");

                // --------------------------------------------
                // Exercise #17 - Creating a simple app with a single variable
                // --------------------------------------------
                Console.WriteLine("$Is your birth month({userBirthMonth}) correct (Yes/No)?");
                userAnswerOfBirthMonth = Console.ReadLine().ToLower();
                if(userAnswerOfBirthMonth == "yes")
                {
                    Console.WriteLine("Thank you!\n\n");
                    isFished = true;
                }
                else
                {
                    Console.WriteLine("The answer is wring. Please type in your birth month again");
                }
            }
        }
    }
}
